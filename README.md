#Hello candidate!

###Introduction
You are asked to add end to end tests to the Tempo Mobile Store website.\
The Tempo Mobile Store is a simple shopping site that will allow customers to browse through a 
certain number of phones and read some information about them.”


###Setup

**Install npm packages:**\
```npm install```

**Start the website:**\
```npm run start```

**Run e2e tests:**\
```npm run test```



###Your Task
using webdriver io, add tests to the following files:\
**test/specs/checkbox.spec.js\
test/specs/filter.spec.js**


###Bonus Tasks (optional)

**Implement the following change to the site**

- When checking some criteria in the left bar, non matching products a currently grayed out. Make them invisible instead.
   - Whenever no product matches the selected criteria, display the message “Sorry, no product matches the selected criteria”.
